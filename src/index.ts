import {MongoClient} from 'mongodb';

async function main() {
    const url = 'mongodb://localhost:27017/testebd';
    try {
        const cliente = await MongoClient.connect(url);
        console.log('Conectado com sucesso');
        const banco = cliente.db('testebd');
        const colecao = banco.collection('testecol');
        /*
        const umaPessoa = {
            nome: 'John Doe',
            idade: 22
        };
        const resultIns = await colecao.insertOne(umaPessoa);
        console.log(`Inserido: ${resultIns.insertedId}`);
        */
        //const cursor = colecao.find({});
        /*
        const cursor = colecao.find({idade: {$gt: 25}});
        console.log(`Contagem: ${await cursor.count()}`);
        */
        /*
        while(await cursor.hasNext()) {
            const documento = await cursor.next();
            console.log(documento);
        }
        */
        //await cursor.forEach(doc => console.log(doc.nome));
        /*
        const resultAlt = await colecao.updateOne({nome: 'Teste'}, {$set: {idade: 45}});
        console.log(`Alteração: ${resultAlt.modifiedCount}`);
        */

        const resultRem = await colecao.deleteOne({nome: 'Teste'});
        console.log(`Remoção: ${resultRem.deletedCount}`);

        await cliente.close();
    } catch(erro) {
        console.log('Erro de acesso ao BD:');
        console.log(erro);
    }
}

main();